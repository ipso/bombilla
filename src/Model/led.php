<?php


namespace App\Model;


class led extends bombilla
{
    private $color; //blanco o rgb
    private $regulador; //true o false
    private const TIPO = "LED";

    public function __construct (string $color, bool $regulador)
    {
        parent::__construct(self::TIPO);
        $this->color = $color;
        $this->regulador = $regulador;
    }

    public function hablar()
    {
        echo "Yo soy una bombilla de tipo".self::TIPO." <br>";
    }

    public function getColor():string{
        return $this->color;
    }

    public function hasRegulador():bool{ //"has" corresponde al inglés Tiene...? Cuando es un booleano es mejor no llamarlo como "get".
        return $this->regulador;
    }
}
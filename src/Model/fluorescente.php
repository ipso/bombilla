<?php


namespace App\Model;


class fluorescente extends bombilla
{
    public function __construct()
    {
        parent::__construct("Fluorescente");
    }

    public function hablar()
    {
        echo "Yo soy una bombilla de tipo :".$this->tipo."<br>";
    }

}
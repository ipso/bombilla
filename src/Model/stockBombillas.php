<?php


namespace App\Model;


class stockBombillas
{
    private const MAX_AMOUNT = 100;
    private $bombillas = [];

    public function guardarBombilla(bombilla $bombilla){
        if ($this->checkHayEspacio()){
            $this->bombillas[$bombilla->getNS()] = $bombilla;
        }
    }

    public function guardarBombillas(array $bombillas){
        foreach ($bombillas as $bombilla){
            $this->guardarBombilla($bombilla);
        }
    }

    public function checkHayEspacio() {
        return count($this->bombillas) < self::MAX_AMOUNT;
    }

    public function getTotalBombillas()
    {
        return count($this->bombillas);
    }

    public function saberSiHayBombilla(){
        return count($this->bombillas) > 0;
    }

    public function sacarUltimaBombillas(){
        if ($this->saberSiHayBombilla()){
            $ultimaPosicion = count($this->bombillas)-1;
            unset($this->bombillas[$ultimaPosicion]);
        }
    }

    public function estocarBombillas(){
        return $this->bombillas;
    }

    public function estocarBombillasxTipo(string $tipo):array { //Tipo la salida con : al final del nombre del método
        $bombillasTipo = [];
        foreach ($this->bombillas as $bombilla){
            if ($bombilla->getTipo() == $tipo){
                $bombillasTipo[] = $bombilla;
            }
        }
        return $bombillasTipo;
    }

    public function buscarBombillaxNS(string $NS):?bombilla{ //El ? por delante puede ser que devuelvas null o bombilla
        return $this->bombillas[$NS];
    }

    public function contarBombillasTipo(){
        $contador = 0;
        foreach ($this->bombillas as $valor){
            if ($valor->getTipo() == "LED"){
                $contador++;
            }
        }
        return $contador;
    }
}
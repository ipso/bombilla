<?php


namespace App\Model;


abstract class bombilla
{
    protected $referencia;
    protected $tipo;
    protected $potencia;
    protected $luminosidad;
    protected $temperatura;
    protected $casquillo;
    protected $NS;


    public function __construct(string $tipo)
    {
        $this->tipo = $tipo;
    }

    abstract function hablar();

    public function setReferencia(string $referencia){
        $this->referencia = $referencia;
    }

    public function setPotencia(string $potencia){
        $this->potencia = $potencia;
    }

    public function setLuminosidad(string $luminosidad){
        $this->luminosidad = $luminosidad;
    }

    public function setTemperatura(string $temperatura){
        $this->temperatura = $temperatura;
    }

    public function setCasquillo(string $casquillo){
        $this->casquillo = $casquillo;
    }

    public function setNS(string $NS){
        $this->NS = $NS;
    }

    public function getTipo(){
        return $this->tipo;
    }

    public function getReferencia(){
        return $this->referencia;
    }

    public function getPotencia(){
        return $this->potencia;
    }

    public function getLuminosidad(){
        return $this->luminosidad;
    }

    public function getTemperatura(){
        return $this->temperatura;
    }

    public function getCasquillo(){
        return $this->casquillo;
    }

    public function getNS(){
        return $this->NS;
    }
}
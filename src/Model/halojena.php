<?php


namespace App\Model;


class halojena extends bombilla
{
    private $regulador;
    private const TIPO = "Halojena";

    public function __construct(bool $regulador)
    {
        parent::__construct(self::TIPO);
        $this->regulador = $regulador;
    }

    public function hablar()
    {
        echo "Yo soy una bombilla de tipo :".self::TIPO."<br>";
    }

    public function hasRegulador():bool{ //"has" corresponde al inglés Tiene...? Cuando es un booleano es mejor no llamarlo como "get".
        return $this->regulador;
    }
}
<?php


namespace App\Controller;


use App\Model\fluorescente;
use App\Model\halojena;
use App\Model\led;
use App\Model\stockBombillas;
use App\Model\bombilla;
use App\View\bombillasListView;

class almacenController
{
    public function Stock(){
        $LED = new led("RGB", true);
        $LED->setReferencia("LED0000");
        $LED->setPotencia("5 W");
        $LED->setCasquillo("GU10");
        $LED->setLuminosidad("510lm");
        $LED->setTemperatura("2800K");
        $LED->setNS("123456789");
        $LED->hablar();

        $Halojena = new halojena(false);
        $Halojena->setReferencia("HAL0000");
        $Halojena->setPotencia("35 W");
        $Halojena->setCasquillo("E27");
        $Halojena->setLuminosidad("1980lm");
        $Halojena->setTemperatura("3300K");
        $Halojena->setNS("953863-k");
        $Halojena->hablar();

        $Fluorescente = new fluorescente();
        $Fluorescente->setReferencia("FLU0000");
        $Fluorescente->setPotencia("30 W");
        $Fluorescente->setCasquillo("G13");
        $Fluorescente->setTemperatura("4500K");
        $Fluorescente->setLuminosidad("800lm");
        $Fluorescente->setNS("814612k2i");
        $Fluorescente->hablar();

        $LED1 = new led("Blanco", false);
        $LED1->setReferencia("LED0001");
        $LED1->setPotencia("3 W");
        $LED1->setCasquillo("GU16");
        $LED1->setLuminosidad("750lm");
        $LED1->setTemperatura("3200K");
        $LED1->setNS("1234d1af");
        $LED1->hablar();

        $LED2 = new led("Blanco", true);
        $LED2->setReferencia("LED0001");
        $LED2->setPotencia("3 W");
        $LED2->setCasquillo("GU16");
        $LED2->setLuminosidad("750lm");
        $LED2->setTemperatura("3200K");
        $LED2->setNS("1524d1af");
        $LED2->hablar();

        $LED3 = new led("RGB",true);
        $LED3->setReferencia("LED0561");
        $LED3->setPotencia("4 W");
        $LED3->setCasquillo("GU5.3");
        $LED3->setLuminosidad("960lm");
        $LED3->setTemperatura("2700");
        $LED3->setNS("ofm34d1af");
        $LED3->hablar();

        $stock = new stockBombillas();
        $stock->guardarBombilla($LED);
        $stock->guardarBombilla($Halojena);
        $stock->guardarBombilla($Fluorescente);
        $stock->guardarBombilla($LED1);
        $stock->guardarBombilla($LED2);
        $stock->guardarBombilla($LED3);

        $LEDs = $stock->estocarBombillasxTipo("LED");
        $Halojenas = $stock->estocarBombillasxTipo("Halojena");

        //$NS = $stock->buscarBombillaxNS("ofm34d1af");

        $view = new bombillasListView();
        $view->print($LEDs);
        //$view->print($Halojenas);
        //$view->print([$NS]);
    }
}